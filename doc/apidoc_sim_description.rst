Simulation description
*******************************



structures
=========================

**structures** contains the main *struct* object, structure generators and helper functions related to the generation of structure geometries.

structure definition class *struct*
--------------------------------------------

.. autoclass:: pyGDM2.structures.struct
   :members:
   :special-members: __init__
   

Structure generators
-----------------------

*rect_wire*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.rect_wire

*rect_dimer*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.rect_dimer

*prism*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.prism

*rhombus*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.rhombus

*hexagon*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.hexagon

*double_hexagon*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.double_hexagon

*diabolo*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.diabolo

*sphere*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.sphere

*nanodisc*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.nanodisc

*nanorod*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.nanorod

*lshape_rect*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.lshape_rect

*lshape_rect_nonsym*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.lshape_rect_nonsym

*lshape_round*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.lshape_round

*split_ring*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.split_ring

*rect_split_ring*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.rect_split_ring



Mesher
-----------------------

*_meshCubic*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures._meshCubic

*_meshHexagonalCompact*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures._meshHexagonalCompact


Other functions
-----------------------

*image_to_struct*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.image_to_struct

*struct_to_image*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.struct_to_image

*get_normalization*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.get_normalization

*center_struct*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.center_struct

*rotate_XY*
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.structures.rotate_XY


materials
=========================

**materials** is a collection of classes defining the dispersion of materials.


*fromFile* class
-----------------------

.. autoclass:: pyGDM2.materials.fromFile
   :members:
   :special-members: __init__

Predefined materials
-----------------------

.. autoclass:: pyGDM2.materials.dummy

.. autoclass:: pyGDM2.materials.gold

.. autoclass:: pyGDM2.materials.alu

.. autoclass:: pyGDM2.materials.silicon





fields
=========================

**fields** contains the main *efield* object and field-generator functions for frequently used fundamental fields.

Incident electro-magnetic field class *efield*
--------------------------------------------------------------------

.. autoclass:: pyGDM2.fields.efield
   :members:
   :special-members: __init__


field generators
-----------------------

plane wave
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.fields.planewave

oblique plane wave for total internal reflection sim.
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.fields.evanescent_planewave

focused plane wave
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.fields.focused_planewave

gaussian
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.fields.gaussian

double gaussian
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.fields.double_gaussian

electric dipolar emitter
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.fields.dipole_electric

magnetic dipolar emitter
++++++++++++++++++++++++++++++++++++++

.. autofunction:: pyGDM2.fields.dipole_magnetic



