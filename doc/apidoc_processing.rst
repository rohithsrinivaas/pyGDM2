Processing
************************************


linear
=========================

**linear** contains evaluation functions for linear effects.

extinction/absorption/scattering cross-sections
-----------------------------------------------------------

.. autofunction:: pyGDM2.linear.extinct

nearfield around structure
-------------------------------------

.. autofunction:: pyGDM2.linear.nearfield

farfield patterns
-------------------------------------

.. autofunction:: pyGDM2.linear.farfield

heat deposition inside structure
-------------------------------------

.. autofunction:: pyGDM2.linear.heat

temperature rise close to structure
-------------------------------------

.. autofunction:: pyGDM2.linear.temperature

dipole decay rate evaluation
-------------------------------------

.. autofunction:: pyGDM2.linear.decay_eval






nonlinear
=========================

**nonlinear** contains evaluation functions for non-linear effects.

TPL and SP-LDOS
-------------------------------------

.. autofunction:: pyGDM2.nonlinear.tpl_ldos



