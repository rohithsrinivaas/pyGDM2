.. raw:: html

    <video autoplay loop poster="_static/overview_static.png">
        <source src="_static/overview.mp4" type="video/mp4">
        <source src="_static/overview.webm" type="video/webm">
        Sorry, your browser doesn't support HTML5 video.
    </video>


.. include:: ../README.rst
