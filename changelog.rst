Changelog
******************

unreleased
=====================


[v1.0.7] - 2018-11-20
=====================
added
--------------
- experimental CUDA support for matrix inversion on GPU (method "cuda")
- structure generators:
    - "prism" now supports truncated edges
    - "spheroid"

fixes
--------------
- MAJOR: fix absolute import error in "visu3d"module, which was broken in former version
- minor fix in struct class, treats lists of wavelengths correctly now (was not affecting pyGDM itself. Failed if a `struct` instance was externally used with a list of wavelengths)


[v1.0.6] - 2018-10-31
=====================
added
--------------
- compatibility with python3 (compatible with python 2 and 3 now)
- default inversion method is now in-place LU decomposition: reduces memory requirement by ~40%
- added some tools to simplify intensity calculation

fixes
--------------
- fix in visu.animate: Works now glitch-less with any user-defined framerate
- minor fix: all classes now initialize with single precision by default. 


[v1.0.5] - 2018-07-9
=====================
fixes
--------------
- critical fix in hdf5 saving / loading. hdf5-data was corrupted during saving/reloading. Works now.

minor
--------------
- by default, multithreading disabled in MPI-version of "scatter". Using SLURM, MPI and pathos seems to conflict which results in major performance drop


[v1.0.4] - 2018-06-07
=====================
added
--------------
- multi-threading support via "thanos" in generalized propagator operations. 
  This drastically increases the speed of raster-scan simulations on multi-core systems.
- hdf5 support for saving/loading simulations
    - doubles the speed for writing, triples speed for reading
    - by default, using "blosc" compression, reduces the filesize by ~ 50%
- hexagonal meshing support in "image_to_struct"
- support for scipy < V0.17 in "decay"


[v1.0.3] - 2018-04-06
=====================
added
--------------
- intallation instructions for MacOSX


[v1.0.2] - 2018-03-29
=====================
added
--------------
- "visu.structure" does automatic multi-structure plots
- compile option for compilation without openmp
- several structure models
- hardcoded silver dielectric function

fixes
--------------
- in "visu.vectorfield_color", fixed an error in the calculation of the field intensity


[v1.0.1] - 2018-02-13
=====================
fixes
--------------
- fixes in "setup.py" script
